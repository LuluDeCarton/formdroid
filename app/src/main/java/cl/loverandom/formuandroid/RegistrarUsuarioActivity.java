package cl.loverandom.formuandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cl.loverandom.formuandroid.controlador.UsuarioControlador;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    public EditText etUserName, etPassword1, etPassword2;
    public TextView tvMensaje;
    public Button btRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        this.etUserName = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.tvMensaje = (TextView) findViewById(R.id.tvMensaje);

        this.btRegistrar = (Button) findViewById(R.id.btRegistrar);

        this.btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = etUserName.getText().toString().trim();
                String password1 = etPassword1.getText().toString().trim();
                String password2 = etPassword2.getText().toString().trim();

                UsuarioControlador capaControlador = new UsuarioControlador(getApplicationContext());
                try {
                    capaControlador.crearUsuario(userName, password1, password2);

                } catch (Exception e){
                    String mensaje = e.getMessage();
                    tvMensaje.setText(mensaje);
                }
            }
        });
    }
}
