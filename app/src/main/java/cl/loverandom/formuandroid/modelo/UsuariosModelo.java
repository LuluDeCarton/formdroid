package cl.loverandom.formuandroid.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModelo {

    private MainDBHelper dbHelper;

    public UsuariosModelo(Context c){
        this.dbHelper = new MainDBHelper(c);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.insert(MainDBContract.MainDBUsuarios.TABLE_NAME, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String uName){
        // Abrir conexion
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // SELECT * FROM usuarios WHERE username = 'david' LIMIT 1
        String[] projection = {
                MainDBContract.MainDBUsuarios._ID,
                MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME,
                MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD
        };

        // Clausulas
        String selection = MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME + " = ? LIMIT 1";
        // Vaores de la clausula
        String[] selectionArgs = { uName };

        // Hacer la consulta a la BD
        Cursor cursor = db.query(
                MainDBContract.MainDBUsuarios.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );

        // Nos movemos a la primera respuesta
        if(cursor.moveToFirst()){
            // Si entramos es por que hay datos

            // Sacar los datos del cursor
            String cursor_username = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME));
            String cursor_password = cursor.getString(cursor.getColumnIndex(MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME, cursor_username);
            usuario.put(MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD, cursor_password);

            return usuario;
        }

        return null;
    }
}
