package cl.loverandom.formuandroid.controlador;

import android.content.ContentValues;
import android.content.Context;

import cl.loverandom.formuandroid.modelo.MainDBContract;
import cl.loverandom.formuandroid.modelo.UsuariosModelo;

public class UsuarioControlador {
    private UsuariosModelo capaModelo;

    public UsuarioControlador(Context ctx){
        this.capaModelo = new UsuariosModelo(ctx);
    }

    public void crearUsuario(String userName, String pass1, String pass2) throws Exception{
        // Validacion de pass
        if(!pass1.equals(pass2)){
            // Nos interesamos en el error
            // Lanzamos el error
            throw new Exception("Las contraseñas no coinciden");
        }

        ContentValues usuario = new ContentValues();
        usuario.put(MainDBContract.MainDBUsuarios.COLUMN_NAME_USERNAME, userName);
        usuario.put(MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD, pass1);

        this.capaModelo.crearUsuario(usuario);
    }

    public int login(String username, String password){
        // Devolver 0 indica que la persona inicio sesion

        // Buscar el usuario en la bd
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorUsername(username);

        // Validar que el usuario existe
        if(usuario == null){
            // El usuario no existe
            return 2;
        }

        // Validar password
        // Tomar password usuario
        String passBD = usuario.getAsString(MainDBContract.MainDBUsuarios.COLUMN_NAME_PASSWORD);
        if(passBD.equals(password)){
            // Login exitoso
            return 0;
        }
        return 1;
    }
}

