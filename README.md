<h1>(Android) "Form Droid"</h1>

<p>"Formdroid" is an application created in Android Studio during the study of the Operating System and creation of applications in the career of Analyst Programmer.
This application was accommodated for 480 x 854 pixels (4.5" screens), and for its construction Lenovo's Vibe B line was used.</p>

<br >

<h2>WARNING!</h2>
<p>1.- This application is in beta phase, and is not stable. Therefore, its development is not finished.</p>
<p>2.- The application has not been restarted and will not be restarted in future versions, which, its reuse for academic purposes or future developments is authorized (giving credits to the creator of the source code "Luciano Sánchez").</p>
<p>3.- According to the general files, this application is based on the Lollipop versions of Android (5.0, API 22). It is not known if it can be reused for previous versions.</p>

<br >

<p>NEVER STOP PROGRAMMING.</p>
<p>Luciano Sánchez</p>

<br >

<p>Analyst Programmer, Inacap </p>
<p>Year 2018 | 4th Semester.</p>

<img src="https://loverandom.cl/wp-content/uploads/2018/12/logo-lrcl-dark.png">